#!/usr/bin/env ruby
require 'nokogiri'
require 'open-uri'

doc = Nokogiri::XML( URI.open( ARGV[0] ) )
rawsummary = doc.search('//itunes:summary')
summary=rawsummary.children.to_a.drop(1)
rawnames = doc.search('//title')
names = rawnames.children.to_a.drop(1).drop(1)
output = names.zip(summary)
output.each do |node|
  puts "\`#{node[0].to_s.gsub("\n"," ").gsub("&amp","and")}\` ~ \`#{node[1].to_s.gsub('<![CDATA[', "").gsub("\n"," ").gsub("&amp","and")}\`" 
end
